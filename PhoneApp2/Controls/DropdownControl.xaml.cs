﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Windows.Media.Capture;
using Badoo.Shared.Extensions;

namespace Badoo.Shared.Controls
{
    using System.Collections;
    using System.Collections.Generic;

    public partial class DropdownControl : UserControl
    {
        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof(object), typeof(DropdownControl), new PropertyMetadata(null, (s, args) => ((DropdownControl)s).SelectedItemChanged()));

        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(DropdownControl), new PropertyMetadata(null, (s, args) => ((DropdownControl)s).ItemsSourceChanged(args)));

        public static readonly DependencyProperty ItemTemplateProperty = DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(DropdownControl), new PropertyMetadata(null));

        public static readonly DependencyProperty BottomAreaProperty = DependencyProperty.Register("BottomArea", typeof(FrameworkElement), typeof(DropdownControl), new PropertyMetadata(null, (s, args) => ((DropdownControl)s).BottomAreaChanged()));
        private bool _isFirstLoadInProgress;

        public FrameworkElement BottomArea
        {
            get { return (FrameworkElement)GetValue(BottomAreaProperty); }
            set { SetValue(BottomAreaProperty, value); }
        }

        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        public IList<ContentControl> Items { get; set; }

        public bool IsExpanded { get; private set; }

        private ContentControl SelectedItemContainer => Items?.FirstOrDefault(x => x.Content?.Equals(SelectedItemInternal) == true);

        private object SelectedItemInternal { get; set; }

        public DropdownControl()
        {
            InitializeComponent();
        }

        public void Expand()
        {
            IsExpanded = true;

            var panelAnimationDuration = 0.3;

            var panelSb = new Storyboard();

            panelSb.Children.Add(AnimationFactory.CreateSlideY(SelectedItemContainer, 0, panelAnimationDuration));
            panelSb.Children.Add(AnimationFactory.CreateSlideY(Panel, 0, panelAnimationDuration));
            panelSb.Children.Add(AnimationFactory.CreateSlideY(BottomArea, 0, panelAnimationDuration));

            var startTime = 0.0;
            double stagger = 0.05;
            foreach (var item in Items)
            {
                if (item != SelectedItemContainer)
                {
                    var animation = AnimationFactory.CreateZoom(item, duration: 0.2D, fadeDuration: 0.2D);
                    //animation.Children.Add(AnimationFactory.CreateSlideY(item, targetTopOffset, panelAnimationDuration));
                    animation.SetBeginTime(startTime);
                    panelSb.Children.Add(animation);

                    startTime += stagger;
                }
            }

            panelSb.Begin();
        }

        private double GetItemOffsetFromTop(FrameworkElement item)
        {
            return item.TransformToVisual(Panel).Transform(new Point()).Y;
        }

        public void Close()
        {
            IsExpanded = false;

            var selectedOffset = GetItemOffsetFromTop(SelectedItemContainer);
            var selectedHeight = SelectedItemContainer.ActualHeight + SelectedItemContainer.Margin.Bottom + SelectedItemContainer.Margin.Top;

            var panelHeight = Panel.ActualHeight;
            var targetPanelOffset = -(panelHeight - selectedHeight);
            var targetTopOffset = panelHeight - selectedHeight - selectedOffset;
            var panelAnimationDuration = 0.3;

            var panelSb = new Storyboard();

            panelSb.Children.Add(AnimationFactory.CreateSlideY(SelectedItemContainer, targetTopOffset, panelAnimationDuration));
            panelSb.Children.Add(AnimationFactory.CreateSlideY(Panel, targetPanelOffset, panelAnimationDuration));
            panelSb.Children.Add(AnimationFactory.CreateSlideY(BottomArea, targetPanelOffset, panelAnimationDuration));

            var startTime = 0.0;
            double stagger = 0.05;
            foreach (var item in Items.Reverse())
            {
                if (item != SelectedItemContainer)
                {
                    var animation = AnimationFactory.CreateZoomOut(item, duration: 0.2D, fadeDuration: 0.2D);
                    //animation.Children.Add(AnimationFactory.CreateSlideY(item, targetTopOffset, panelAnimationDuration));
                    animation.SetBeginTime(startTime);
                    panelSb.Children.Add(animation);

                    startTime += stagger;
                }
            }

            panelSb.Begin();
        }

        private void ItemsSourceChanged(DependencyPropertyChangedEventArgs args)
        {
            Panel.Children.Clear();

            Items = new List<ContentControl>();

            if (args.NewValue != null)
            {
                foreach (var item in ItemsSource)
                {
                    var container = GetContainer();
                    container.ContentTemplate = ItemTemplate;
                    container.Content = item;
                    Items.Add(container);
                    Panel.Children.Add(container);
                }

                if (SelectedItem == null)
                {
                    SelectedItemInternal = ItemsSource.OfType<object>().FirstOrDefault();
                }

                OnFirstLoad();
            }
        }

        private void OnFirstLoad()
        {
            _isFirstLoadInProgress = true;

            Action<Size> onMeasured = null;
            onMeasured = size =>
            {
                Panel.MeasureCompleted -= onMeasured;

                var height = size.Height;
                Panel.GetCompositeTransform().TranslateY = -height;
                var dsize = SelectedItemContainer.DesiredSize;
                var ah = SelectedItemContainer.ActualHeight;

                _isFirstLoadInProgress = false;
            };

            Panel.MeasureCompleted += onMeasured;
        }

        private void SelectedItemChanged()
        {
            if (!_isFirstLoadInProgress)
            {
                SelectedItemInternal = SelectedItem ?? SelectedItemInternal;
                TryUpdateSelectedItem();
            }
        }

        private void TryUpdateSelectedItem()
        {
            if (SelectedItemContainer != null)
            {
                if (IsExpanded)
                {
                    Close();
                }
                else
                {
                    if (!IsOnTop(SelectedItemContainer))
                    {
                        AnimateOnTop(Items, SelectedItemContainer);
                    }
                }
            }
        }

        private ContentControl GetContainer()
        {
            var container = new ContentControl
            {
                HorizontalContentAlignment = HorizontalAlignment.Stretch,
                VerticalContentAlignment = VerticalAlignment.Stretch,
                //Opacity = 0,
                IsHitTestVisible = false,
                CacheMode = new BitmapCache(),
            };

            container.GetCompositeTransform();

            MarkBackground(container);

            return container;
        }

        private void MarkTop(FrameworkElement item)
        {
            Canvas.SetZIndex(item, 1);
        }

        private void MarkBackground(FrameworkElement item)
        {
            Canvas.SetZIndex(item, 0);
        }

        private bool IsOnTop(FrameworkElement item)
        {
            return Canvas.GetZIndex(item) > 0;
        }

        private void AnimateOnTop(IEnumerable<FrameworkElement> allItems, FrameworkElement topItem)
        {
            var currentTop = Items.FirstOrDefault(IsOnTop);

            foreach (var item in allItems)
            {
                if (item != currentTop)
                {
                    item.Opacity = 0;
                    MarkBackground(item);
                }
            }

            if (currentTop != null)
            {
                var outSb = AnimationFactory.CreateZoomOut(currentTop, duration: 0.1D, fadeDuration: 0.1D);
                outSb.Completed += (sender, args) =>
                {
                    currentTop.Opacity = 0;
                    MarkBackground(currentTop);
                };
                outSb.Begin();
            }


            MarkTop(topItem);
            topItem.Opacity = 0;
            var inSb = AnimationFactory.CreateZoom(topItem, duration: 0.1D, fadeDuration: 0.1D, startOpacity: 0D);
            inSb.Begin();
        }

        private void BottomAreaChanged()
        {
            BottomArea.CacheMode = new BitmapCache();
        }
    }
}
