﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Badoo.Shared.Extensions
{
    public class CustomStackPanel : Panel
    {
        public event Action<Size> MeasureCompleted;

        protected override Size MeasureOverride(Size availableSize)
        {
            var height = 0D;
            var maxWidth = 0D;
            foreach (var child in Children)
            {
                child.Measure(availableSize);
                height += child.DesiredSize.Height;
                maxWidth = Math.Max(maxWidth, child.DesiredSize.Width);
            }

            var finalSize = new Size(maxWidth, height);

            MeasureCompleted?.Invoke(finalSize);

            return finalSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            var heightTaken = 0D;
            foreach (var child in Children)
            {
                child.Arrange(new Rect(0, heightTaken, finalSize.Width, child.DesiredSize.Height));
                heightTaken += child.DesiredSize.Height;
            }

            return new Size(finalSize.Width, finalSize.Height);
        }
    }
}