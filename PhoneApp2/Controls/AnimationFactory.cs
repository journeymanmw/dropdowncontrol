using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Badoo.Shared.Extensions
{
    public static class AnimationFactory
    {
        public class Easings
        {
            Easings() { }

            public static ExponentialEase Expo6Out { get; } = new ExponentialEase { EasingMode = EasingMode.EaseOut, Exponent = 6 };
            public static ExponentialEase Expo6In { get; } = new ExponentialEase { EasingMode = EasingMode.EaseIn, Exponent = 6 };
            public static ExponentialEase Expo4Out { get; } = new ExponentialEase { EasingMode = EasingMode.EaseOut, Exponent = 4 };
            public static ExponentialEase Expo4In { get; } = new ExponentialEase { EasingMode = EasingMode.EaseIn, Exponent = 4 };
            public static ExponentialEase Expo15In { get; } = new ExponentialEase { EasingMode = EasingMode.EaseIn, Exponent = 15 };

            public static CircleEase CircleOut { get; } = new CircleEase { EasingMode = EasingMode.EaseOut };
        }

        public static Storyboard CreateSoftSlideUpFadeIn(FrameworkElement element)
        {
            Storyboard sb = new Storyboard();
            var duration = 0.35d;

            var dbx1 = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(element, TargetProperty.CompositeTransformTranslateY);
            dbx1.AddEasingDoubleKeyFrame(0, 40);
            dbx1.AddEasingDoubleKeyFrame(duration, 0, Easings.CircleOut);

            var op1 = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(element, TargetProperty.Opacity);
            op1.AddEasingDoubleKeyFrame(0, 0, Easings.Expo6Out);
            op1.AddEasingDoubleKeyFrame(duration, 1, Easings.CircleOut);

            return sb;
        }


        /// <summary>
        /// Creates a zoom in / out animation between two elements
        /// </summary>
        /// <param name="first">Start element</param>
        /// <param name="second">End element</param>
        /// <param name="isForward">If true, elements zoom upwards. Else elements zoom downwards.</param>
        /// <param name="disableHitTesting">If true, elements have hit testing disabled during the animation</param>
        /// <param name="dependents">Additional elements for the animation. These elements will have their visibilities set to visible. Useful for visual state transitions.</param>
        /// <returns></returns>
        public static Storyboard CreateZoomBetween(FrameworkElement first, FrameworkElement second, bool isForward, bool disableHitTesting, params FrameworkElement[] dependents)
        {
            double outDuration = 0.3;
            double inDuration = isForward ? 0.6 : 0.4;

            double startScale = isForward ? 0.7 : 1.3;
            double endScale = isForward ? 1.3 : 0.7;

            Storyboard sb = new Storyboard();

            // 1. Animate Out
            var vis1 = sb.CreateTimeline<ObjectAnimationUsingKeyFrames>(first, TargetProperty.Visiblity);
            vis1.AddDiscreteObjectKeyFrame(0, Visibility.Visible);

            var dbox = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(first, TargetProperty.CompositeTransformScaleX);
            dbox.AddEasingDoubleKeyFrame(outDuration, endScale, Easings.Expo6In);

            var dboy = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(first, TargetProperty.CompositeTransformScaleY);
            dboy.AddEasingDoubleKeyFrame(outDuration, endScale, Easings.Expo6In);

            var opo = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(first, TargetProperty.Opacity);
            opo.AddEasingDoubleKeyFrame(0, 1, Easings.Expo6Out);
            opo.AddEasingDoubleKeyFrame(outDuration, 0, Easings.Expo6In);

            // 2. Animate new in
            var vis2 = sb.CreateTimeline<ObjectAnimationUsingKeyFrames>(second, TargetProperty.Visiblity);
            vis2.AddDiscreteObjectKeyFrame(0, Visibility.Visible);

            var dbix = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(second, TargetProperty.CompositeTransformScaleX);
            dbix.AddEasingDoubleKeyFrame(0, startScale);
            dbix.AddEasingDoubleKeyFrame(outDuration, startScale);
            dbix.AddEasingDoubleKeyFrame(outDuration + inDuration, 1, Easings.Expo6Out);

            var dbyx = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(second, TargetProperty.CompositeTransformScaleY);
            dbyx.AddEasingDoubleKeyFrame(0, startScale);
            dbyx.AddEasingDoubleKeyFrame(outDuration, startScale);
            dbyx.AddEasingDoubleKeyFrame(outDuration + inDuration, 1, Easings.Expo6Out);

            var opi = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(second, TargetProperty.Opacity);
            opi.AddEasingDoubleKeyFrame(0, 0);
            opi.AddEasingDoubleKeyFrame(outDuration, 0);
            opi.AddEasingDoubleKeyFrame(outDuration + inDuration, 1, Easings.Expo6Out);

            // 3. Handle additional options
            foreach (var dependent in dependents)
            {
                var visd = sb.CreateTimeline<ObjectAnimationUsingKeyFrames>(dependent, TargetProperty.Visiblity);
                visd.AddDiscreteObjectKeyFrame(0, Visibility.Visible);
            }

            if (disableHitTesting)
            {
                var ht1 = sb.CreateTimeline<ObjectAnimationUsingKeyFrames>(first, TargetProperty.IsHitTestVisible);
                ht1.AddDiscreteObjectKeyFrame(0, false);
                ht1.AddDiscreteObjectKeyFrame(outDuration + inDuration, first.IsHitTestVisible);

                var ht2 = sb.CreateTimeline<ObjectAnimationUsingKeyFrames>(second, TargetProperty.IsHitTestVisible);
                ht2.AddDiscreteObjectKeyFrame(0, false);
                // We allow hit testing a little early here as the animation has an intentional slow finish
                ht2.AddDiscreteObjectKeyFrame(outDuration + (inDuration / 2.5), second.IsHitTestVisible);
            }

            return sb;
        }

        public static Storyboard CreateSlideBetween(FrameworkElement first, FrameworkElement second, bool isForward, params FrameworkElement[] dependents)
        {
            var ct = first.GetCompositeTransform();
            var ct2 = second.GetCompositeTransform();

            double outDuration = 0.25;
            double inDuration = 0.5;
            double delay = 0.1;
            TimeSpan delaySpan = TimeSpan.FromSeconds(delay);

            double outEnd = isForward ? -150 : 150;
            double inStart = isForward ? 150 : -150;

            Storyboard sb = new Storyboard();

            foreach (var dependent in dependents)
            {
                var visd = sb.CreateTimeline<ObjectAnimationUsingKeyFrames>(dependent, TargetProperty.Visiblity);
                visd.AddDiscreteObjectKeyFrame(0, Visibility.Visible);
            }

            // Animate first pane out
            var vis1 = sb.CreateTimeline<ObjectAnimationUsingKeyFrames>(first, TargetProperty.Visiblity);
            vis1.AddDiscreteObjectKeyFrame(0, Visibility.Visible);

            var dbx1 = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(first, TargetProperty.CompositeTransformTranslateX);
            dbx1.BeginTime = delaySpan;
            dbx1.AddEasingDoubleKeyFrame(outDuration, outEnd, Easings.Expo6In);

            var op1 = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(first, TargetProperty.Opacity);
            op1.BeginTime = delaySpan;
            op1.AddEasingDoubleKeyFrame(outDuration, 0, Easings.Expo6In);

            // Animate second pane in
            var vis2 = sb.CreateTimeline<ObjectAnimationUsingKeyFrames>(second, TargetProperty.Visiblity);
            vis2.AddDiscreteObjectKeyFrame(0, Visibility.Visible);

            var dx2 = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(second, TargetProperty.CompositeTransformTranslateX);
            dx2.AddEasingDoubleKeyFrame(0, inStart);
            dx2.AddEasingDoubleKeyFrame(delay + outDuration, inStart);
            dx2.AddEasingDoubleKeyFrame(outDuration + delay + inDuration, 0, Easings.Expo6Out);

            var op2 = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(second, TargetProperty.Opacity);
            op2.AddEasingDoubleKeyFrame(0, 0);
            op2.AddEasingDoubleKeyFrame(delay + outDuration, 0);
            op2.AddEasingDoubleKeyFrame(outDuration + delay + inDuration, 1, Easings.Expo6Out);

            return sb;
        }

        public static Storyboard CreateSlideY(FrameworkElement element, double toOffset, double duration)
        {
            Storyboard sb = new Storyboard();

            var dbx1 = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(element, TargetProperty.CompositeTransformTranslateY);
            dbx1.AddEasingDoubleKeyFrame(duration, toOffset, Easings.Expo6Out);

            return sb;
        }

        public static Storyboard CreateZoom(
            FrameworkElement element,
            double fromScale = 0.8d,
            double endScale = 1d,
            double duration = 0.7,
            double startOpacity = 0,
            double endOpacity = 1,
            double fadeDuration = 0.15,
            EasingFunctionBase ease = null)
        {
            Storyboard sb = new Storyboard();

            Array.ForEach(TargetProperty.CompositeTransformScaleXY, target =>
            {
                var sc = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(element, target);
                sc.AddDiscreteDoubleKeyFrame(0, fromScale);
                sc.AddEasingDoubleKeyFrame(duration, endScale, ease ?? Easings.Expo6Out);
            });

            var op = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(element, TargetProperty.Opacity);
            op.AddDiscreteDoubleKeyFrame(0, startOpacity);
            op.AddEasingDoubleKeyFrame(fadeDuration, endOpacity);

            return sb;
        }

        public static Storyboard CreateZoomOut(
            FrameworkElement element,
            double fromScale = 1d,
            double endScale = 0.8d,
            double duration = 0.2d,
            double startOpacity = 1d,
            double endOpacity = 0d,
            double fadeDuration = 0.2d,
            EasingFunctionBase ease = null)
        {
            return CreateZoom(element, fromScale, endScale, duration, startOpacity, endOpacity, fadeDuration, ease ?? Easings.Expo4In);
        }

        public static Storyboard CreateFade(FrameworkElement element, double to, double duration = 0.1)
        {
            Storyboard sb = new Storyboard();

            var o = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(element, TargetProperty.Opacity);
            o.AddEasingDoubleKeyFrame(duration, to);

            return sb;
        }




        //------------------------------------------------------
        //
        //  NATIVE TRANSITIONS
        //
        //------------------------------------------------------
        /* These transition feature easings and timings that are native to Windows Phone OS */

        //------------------------------------------------------
        //
        //  Slide
        //
        //------------------------------------------------------

        public static Storyboard CreateSlideUpFadeIn(FrameworkElement element)
        {
            Storyboard sb = new Storyboard();
            var duration = 0.3d;

            var dbx1 = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(element, TargetProperty.CompositeTransformTranslateY);
            dbx1.AddEasingDoubleKeyFrame(0, 200);
            dbx1.AddEasingDoubleKeyFrame(duration, 0, Easings.Expo6Out);

            var op1 = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(element, TargetProperty.Opacity);
            op1.AddEasingDoubleKeyFrame(0, 0, Easings.Expo6Out);
            op1.AddEasingDoubleKeyFrame(duration, 1, Easings.Expo6Out);

            return sb;
        }

        public static Storyboard CreateSlideDownFadeOut(FrameworkElement element, Storyboard sb = null)
        {
            sb = sb ?? new Storyboard();
            var duration = 0.25d;

            var dbx1 = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(element, TargetProperty.CompositeTransformTranslateY);
            dbx1.AddEasingDoubleKeyFrame(0, 0);
            dbx1.AddEasingDoubleKeyFrame(duration, 200, Easings.Expo6Out);

            var op1 = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(element, TargetProperty.Opacity);
            op1.AddEasingDoubleKeyFrame(0, 1);
            op1.AddEasingDoubleKeyFrame(duration, 0, Easings.Expo6Out);

            return sb;
        }



        //------------------------------------------------------
        //
        //  Swivel
        //
        //------------------------------------------------------

        public static Storyboard CreateSwivelBackwardIn(FrameworkElement foreground, FrameworkElement background = null, bool fadeBackground = false, Storyboard sb = null)
        {
            sb = sb ?? new Storyboard();

            double popupDelay = 0.1;
            double projectDuration = 0.35;

            // 1. Ensure background visible
            if (background != null)
            {
                var vis1 = sb.CreateTimeline<ObjectAnimationUsingKeyFrames>(background, TargetProperty.Visiblity);
                vis1.AddDiscreteObjectKeyFrame(0, Visibility.Visible);

                if (fadeBackground)
                {
                    var opo = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(background, TargetProperty.Opacity);
                    opo.AddDiscreteDoubleKeyFrame(0, 0);
                    opo.AddDiscreteDoubleKeyFrame(popupDelay, 0);
                    opo.AddEasingDoubleKeyFrame(popupDelay + 0.05, 1);
                }
                else
                {
                    var opo = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(background, TargetProperty.Opacity);
                    opo.AddDiscreteDoubleKeyFrame(0, 1);
                }

            }

            // 2. Swivel foreground inwards
            var opc = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(foreground, TargetProperty.Opacity);
            opc.AddDiscreteDoubleKeyFrame(0, 0);
            opc.AddDiscreteDoubleKeyFrame(popupDelay, 1);

            var px = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(foreground, TargetProperty.ProjectionRotationX);
            px.AddDiscreteDoubleKeyFrame(0, -45);
            px.AddDiscreteDoubleKeyFrame(popupDelay, -45);
            px.AddEasingDoubleKeyFrame(popupDelay + projectDuration, 0, Easings.Expo6Out);

            return sb;
        }

        public static Storyboard CreateSwivelForwardOut(FrameworkElement popupContent, FrameworkElement popupBackground = null, Storyboard sb = null)
        {
            sb = sb ?? new Storyboard();
            double duration = 0.25;

            // 1. Swivel foreground content outwards
            var px = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(popupContent, TargetProperty.ProjectionRotationX);
            px.AddEasingDoubleKeyFrame(duration, 90, Easings.Expo15In);

            var op = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(popupContent, TargetProperty.Opacity);
            op.AddDiscreteDoubleKeyFrame(duration - 0.01, 1);
            op.AddEasingDoubleKeyFrame(duration, 0);

            // 2. Ensure background visible
            if (popupBackground != null)
            {
                var opo = sb.CreateTimeline<DoubleAnimationUsingKeyFrames>(popupBackground, TargetProperty.Opacity);
                opo.AddDiscreteDoubleKeyFrame(duration, 1);
                opo.AddEasingDoubleKeyFrame(duration + 0.05, 0);
            }

            return sb;
        }



    }
}