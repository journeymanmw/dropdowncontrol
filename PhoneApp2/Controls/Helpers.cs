﻿namespace Badoo.Shared.Extensions
{
    using System;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Controls;
    using System.Linq;

    public static class MyExtensions
    {
        public static readonly DependencyProperty AnimatableVisibilityProperty = DependencyProperty.RegisterAttached("AnimatableVisibility", typeof(Visibility?), typeof(MyExtensions), new PropertyMetadata(null, OnAnimatedVisibilityChanged));

        public static Visibility GetAnimatableVisibility(DependencyObject obj)
        {
            return (Visibility)obj.GetValue(AnimatableVisibilityProperty);
        }

        public static void SetAnimatableVisibility(DependencyObject obj, Visibility value)
        {
            obj.SetValue(AnimatableVisibilityProperty, value);
        }

        public static Storyboard GetCollapsedToVisibleStoryboard(DependencyObject obj)
        {
            return (Storyboard)obj.GetValue(CollapsedToVisibleStoryboardProperty);
        }

        public static void SetCollapsedToVisibleStoryboard(DependencyObject obj, Storyboard value)
        {
            obj.SetValue(CollapsedToVisibleStoryboardProperty, value);
        }

        public static readonly DependencyProperty CollapsedToVisibleStoryboardProperty = DependencyProperty.RegisterAttached("CollapsedToVisibleStoryboard", typeof(Storyboard), typeof(MyExtensions), new PropertyMetadata(null));



        public static Storyboard GetVisibleToCollapsedStoryboard(DependencyObject obj)
        {
            return (Storyboard)obj.GetValue(VisibleToCollapsedStoryboardProperty);
        }

        public static void SetVisibleToCollapsedStoryboard(DependencyObject obj, Storyboard value)
        {
            obj.SetValue(VisibleToCollapsedStoryboardProperty, value);
        }

        public static readonly DependencyProperty VisibleToCollapsedStoryboardProperty = DependencyProperty.RegisterAttached("VisibleToCollapsedStoryboard", typeof(Storyboard), typeof(MyExtensions), new PropertyMetadata(null));

        private static async void OnAnimatedVisibilityChanged(DependencyObject s, DependencyPropertyChangedEventArgs args)
        {
            var sender = (FrameworkElement)s;
            var newVisibility = (Visibility?)args.NewValue;
            if (newVisibility == Visibility.Visible)
            {
                sender.Opacity = 0;
                sender.Visibility = Visibility.Visible;
                var sb = GetCollapsedToVisibleStoryboard(sender);
                sb = sb ?? AnimationFactory.CreateZoom(sender, fromScale: 0D, duration: 0.1D, fadeDuration: 0.1D);
                await sb.BeginAsync();
            }
            else
            {
                var sb = GetVisibleToCollapsedStoryboard(sender);
                sb = sb ?? AnimationFactory.CreateZoomOut(sender, endScale: 0D, duration: 0.1D, fadeDuration: 0.1D);
                await sb.BeginAsync();
                sender.Visibility = Visibility.Collapsed;
            }
        }
    }

    /// <summary>
    /// A collection of useful animation-focused extension methods
    /// </summary>
    public static class AnimationExtensions
    {
        #region Composite Transform

        public static CompositeTransform GetNewCompositeTransform(this FrameworkElement element, bool centreOriginOnCreation = true, bool overwriteOtherTransforms = true)
        {
            element.RenderTransform = null;
            return element.GetCompositeTransform(centreOriginOnCreation, overwriteOtherTransforms);
        }

        public static CompositeTransform GetCompositeTransform(this FrameworkElement element, bool centreOriginOnCreation = true, bool overwriteOtherTransforms = true)
        {
            CompositeTransform ct = null;

            ct = element.RenderTransform as CompositeTransform;

            if (ct != null)
            {
                return ct;
            }

            // 3. If there's nothing there, create a new CompositeTransform
            if (element.RenderTransform == null)
            {
                element.RenderTransform = new CompositeTransform();
                ct = (CompositeTransform)element.RenderTransform;
                if (centreOriginOnCreation)
                {
                    ct.CenterX = ct.CenterY = 0.5;
                    element.RenderTransformOrigin = new Point(0.5, 0.5);
                }
            }
            else
            {
                ct = new CompositeTransform();
                if (centreOriginOnCreation)
                {
                    ct.CenterX = ct.CenterY = 0.5;
                    element.RenderTransformOrigin = new Point(0.5, 0.5);
                }

                // 5. See if the existing item is a singular transform, and convert it to a CompositeTransform
                var transform = element.RenderTransform as Transform;
                if (transform != null)
                {
                    ApplyTransform(ref ct, transform);
                    element.RenderTransform = ct;
                }
                else
                {
                    // 6. If we're a group of transforms, convert each child individually
                    var group = element.RenderTransform as TransformGroup;
                    if (group != null)
                    {
                        foreach (var tran in group.Children)
                        {
                            ApplyTransform(ref ct, tran);
                        }

                        element.RenderTransform = ct;
                    }
                }
            }

            return ct;
        }

        /// <summary>
        /// Adds the effect of a regular transform to a composite transform
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="t"></param>
        internal static void ApplyTransform(ref CompositeTransform ct, Transform t)
        {
            if (t is TranslateTransform)
            {
                var tt = (TranslateTransform)t;
                ct.TranslateX = tt.X;
                ct.TranslateY = tt.Y;
            }
            else if (t is RotateTransform)
            {
                var rt = (RotateTransform)t;
                ct.Rotation = rt.Angle;
                ct.CenterX = rt.CenterX;
                ct.CenterY = rt.CenterY;
            }
            else if (t is SkewTransform)
            {
                var sK = (SkewTransform)t;
                ct.SkewX = sK.AngleX;
                ct.SkewY = sK.AngleY;
                ct.CenterX = sK.CenterX;
                ct.CenterY = sK.CenterY;
            }
            else if (t is ScaleTransform)
            {
                var sc = (ScaleTransform)t;
                ct.ScaleX = sc.ScaleX;
                ct.ScaleY = sc.ScaleY;
                ct.CenterX = sc.CenterX;
                ct.CenterY = sc.CenterY;
            }
        }

        #endregion

        #region Plane Projection

        /// <summary>
        /// Gets the plane projection from a FrameworkElement's projection property. If 
        /// the property is null or not a plane projection, a new plane projection is created
        /// and set as the plane projection and then returned
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static PlaneProjection GetPlaneProjection(this FrameworkElement element)
        {
            PlaneProjection projection = null;

            projection = element.Projection as PlaneProjection;
            if (projection == null)
            {
                element.Projection = new PlaneProjection();
                projection = (PlaneProjection)element.Projection;
            }

            return projection;
        }

        #endregion

        #region Storyboard

        /// <summary>
        /// Sets the begin time on a storyboard and then returns the same storyboard
        /// </summary>
        /// <param name="storyboard"></param>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static Storyboard SetBeginTime(this Storyboard storyboard, double seconds)
        {
            storyboard.BeginTime = TimeSpan.FromSeconds(seconds);
            return storyboard;
        }

        /// <summary>
        /// Returns an await-able task that runs the storyboard through to completion
        /// </summary>
        /// <param name="storyboard"></param>
        /// <returns></returns>
        public static Task BeginAsync(this Storyboard storyboard)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
            if (storyboard == null)
            {
                tcs.SetException(new ArgumentNullException());
            }
            else
            {
                EventHandler onComplete = null;
                onComplete = (s, e) =>
                {
                    storyboard.Completed -= onComplete;
                    tcs.SetResult(true);
                };
                storyboard.Completed += onComplete;
                storyboard.Begin();
            }

            return tcs.Task;
        }

        public static void AddTimeline(this Storyboard storyboard, Timeline timeline, FrameworkElement target, string targetProperty)
        {
            if (targetProperty.In(
                TargetProperty.CompositeTransformRotation,
                TargetProperty.CompositeTransformScaleX, TargetProperty.CompositeTransformScaleY,
                TargetProperty.CompositeTransformSkewX, TargetProperty.CompositeTransformSkewY,
                TargetProperty.CompositeTransformTranslateX, TargetProperty.CompositeTransformTranslateY))
            {
                GetCompositeTransform(target);
            }
            else if (targetProperty.In(
                TargetProperty.ProjectionGlobalOffsetX, TargetProperty.ProjectionGlobalOffsetY, TargetProperty.ProjectionGlobalOffsetZ,
                TargetProperty.ProjectionRotationX, TargetProperty.ProjectionRotationY, TargetProperty.ProjectionRotationZ))
            {
                GetPlaneProjection(target);
            }

            Storyboard.SetTarget(timeline, target);
            Storyboard.SetTargetProperty(timeline, new PropertyPath(targetProperty));

            storyboard.Children.Add(timeline);
        }

        #endregion

        #region Timelines

        public static void AddEasingDoubleKeyFrame(this DoubleAnimationUsingKeyFrames doubleAnimation, double seconds, double value, EasingFunctionBase ease = null)
        {
            doubleAnimation.AddEasingDoubleKeyFrame(TimeSpan.FromSeconds(seconds), value, ease);
        }

        public static void AddEasingDoubleKeyFrame(this DoubleAnimationUsingKeyFrames doubleAnimation, TimeSpan time, double value, EasingFunctionBase ease = null)
        {
            doubleAnimation.KeyFrames.Add(new EasingDoubleKeyFrame
            {
                KeyTime = KeyTime.FromTimeSpan(time),
                Value = value,
                EasingFunction = ease
            });
        }

        public static void AddDiscreteDoubleKeyFrame(this DoubleAnimationUsingKeyFrames doubleAnimation, double seconds, double value)
        {
            doubleAnimation.AddDiscreteDoubleKeyFrame(TimeSpan.FromSeconds(seconds), value);
        }

        public static void AddDiscreteDoubleKeyFrame(this DoubleAnimationUsingKeyFrames doubleAnimation, TimeSpan time, double value)
        {
            doubleAnimation.KeyFrames.Add(new DiscreteDoubleKeyFrame
            {
                KeyTime = KeyTime.FromTimeSpan(time),
                Value = value
            });
        }

        public static void AddSplineDoubleKeyFrame(this DoubleAnimationUsingKeyFrames doubleAnimation, double seconds, double value, KeySpline spline = null)
        {
            doubleAnimation.AddSplineDoubleKeyFrame(TimeSpan.FromSeconds(seconds), value, spline);
        }

        public static void AddSplineDoubleKeyFrame(this DoubleAnimationUsingKeyFrames doubleAnimation, TimeSpan time, double value, KeySpline spline = null)
        {
            doubleAnimation.KeyFrames.Add(new SplineDoubleKeyFrame
            {
                KeyTime = KeyTime.FromTimeSpan(time),
                Value = value,
                KeySpline = spline
            });
        }

        public static void AddDiscreteObjectKeyFrame(this ObjectAnimationUsingKeyFrames objectAnimation, double seconds, object value)
        {
            objectAnimation.AddDiscreteObjectKeyFrame(TimeSpan.FromSeconds(seconds), value);
        }

        public static void AddDiscreteObjectKeyFrame(this ObjectAnimationUsingKeyFrames objectAnimation, TimeSpan time, object value)
        {
            objectAnimation.KeyFrames.Add(new DiscreteObjectKeyFrame
            {
                KeyTime = KeyTime.FromTimeSpan(time),
                Value = value
            });
        }

        public static T CreateTimeline<T>(FrameworkElement target, string targetProperty, Storyboard parent)
            where T : Timeline, new()
        {
            T timeline = new T();
            parent.AddTimeline(timeline, target, targetProperty);
            return timeline;
        }

        public static T CreateTimeline<T>(this Storyboard parent, FrameworkElement target, string targetProperty)
            where T : Timeline, new()
        {
            T timeline = new T();
            parent.AddTimeline(timeline, target, targetProperty);
            return timeline;
        }

        #endregion

        public static KeySpline CreateKeySpline(double x1, double y1, double x2, double y2)
        {
            KeySpline keyspline = new KeySpline();
            keyspline.SetPoints(x1, y1, x2, y2);
            return keyspline;
        }

        public static void SetPoints(this KeySpline keySpline, double x1, double y1, double x2, double y2)
        {
            keySpline.ControlPoint1 = new Point(x1, y1);
            keySpline.ControlPoint2 = new Point(x2, y2);
        }

        public static Task WaitForLoadedAsync(this FrameworkElement element, bool tryApplyTemplate = false)
        {
            TaskCompletionSource<FrameworkElement> tcs = new TaskCompletionSource<FrameworkElement>();
            RoutedEventHandler loaded = null;

            loaded = (s, e) =>
            {
                var ele = (FrameworkElement)s;
                ele.Loaded -= loaded;
                tcs.SetResult(ele);
            };

            element.Loaded += loaded;

            if (tryApplyTemplate)
                (element as Control)?.ApplyTemplate();

            return tcs.Task;
        }

        #region Attached Properties

        public static int GetAnimationGroup(DependencyObject obj)
        {
            return (int)obj.GetValue(AnimationGroupProperty);
        }

        public static void SetAnimationGroup(DependencyObject obj, int value)
        {
            obj.SetValue(AnimationGroupProperty, value);
        }

        public static readonly DependencyProperty AnimationGroupProperty =
            DependencyProperty.RegisterAttached("AnimationGroup", typeof(int), typeof(FrameworkElement), new PropertyMetadata(0));

        #region ShouldAnimate

        /// <summary>
        /// Used by Preset Animations. If false, element will not be animated and will not affect stagger timings.
        /// </summary>
        public static bool GetShouldAnimate(DependencyObject obj)
        {
            return (bool)obj.GetValue(ShouldAnimateProperty);
        }

        /// <summary>
        /// Used by Preset Animations. If false, element will not be animated and will not affect stagger timings.
        /// </summary>
        public static void SetShouldAnimate(DependencyObject obj, bool value)
        {
            obj.SetValue(ShouldAnimateProperty, value);
        }

        /// <summary>
        /// Used by Preset Animations. If false, element will not be animated and will not affect stagger timings.
        /// </summary>
        public static readonly DependencyProperty ShouldAnimateProperty =
            DependencyProperty.RegisterAttached("ShouldAnimate", typeof(bool), typeof(FrameworkElement), new PropertyMetadata(true));

        #endregion

        #region AddedDelay

        /// <summary>
        /// Used by Preset Animations. If false, element will not be animated and will not affect stagger timings.
        /// </summary>
        public static TimeSpan GetAddedDelay(DependencyObject obj)
        {
            return (TimeSpan)obj.GetValue(AddedDelayProperty);
        }

        /// <summary>
        /// Used by Preset Animations. If false, element will not be animated and will not affect stagger timings.
        /// </summary>
        public static void SetAddedDelay(DependencyObject obj, TimeSpan value)
        {
            obj.SetValue(AddedDelayProperty, value);
        }

        /// <summary>
        /// Used by Preset Animations. Allows you to specific an additional stagger delay for a single element
        /// </summary>
        public static readonly DependencyProperty AddedDelayProperty =
            DependencyProperty.RegisterAttached("AddedDelay", typeof(TimeSpan), typeof(FrameworkElement), new PropertyMetadata(TimeSpan.Zero));

        #endregion

        #endregion
    }

    public static class TargetProperty
    {
        //------------------------------------------------------
        //
        // UI Element
        //
        //------------------------------------------------------

        // UIElement Opacity
        public static string Opacity = "(UIElement.Opacity)";
        // UIElement Visibility
        public static string Visiblity = "(UIElement.Visibility)";
        // UIElement IsHitTestVisible
        public static string IsHitTestVisible = "(UIElement.IsHitTestVisible)";
        // UIElement IsEnabled
        public static string IsEnabled = "(UIElement.IsEnabled)";




        //------------------------------------------------------
        //
        // Composite Transform (Render Transform)
        //
        //------------------------------------------------------

        // Render Transform Composite Transform X-Axis Translation
        public static string CompositeTransformTranslateX = "(UIElement.RenderTransform).(CompositeTransform.TranslateX)";
        // Render Transform Composite Transform Y-Axis Translation
        public static string CompositeTransformTranslateY = "(UIElement.RenderTransform).(CompositeTransform.TranslateY)";
        // Render Transform Composite Transform X-Axis Scale
        public static string CompositeTransformScaleX = "(UIElement.RenderTransform).(CompositeTransform.ScaleX)";
        // Render Transform Composite Transform Y-Axis Scale
        public static string CompositeTransformScaleY = "(UIElement.RenderTransform).(CompositeTransform.ScaleY)";
        // Render Transform Composite Transform X-Scale Skew
        public static string CompositeTransformSkewX = "(UIElement.RenderTransform).(CompositeTransform.SkewX)";
        // Render Transform Composite Transform Y-Scale Skew
        public static string CompositeTransformSkewY = "(UIElement.RenderTransform).(CompositeTransform.SkewY)";
        // Render Transform Composite Transform Rotation
        public static string CompositeTransformRotation = "(UIElement.RenderTransform).(CompositeTransform.Rotation)";
        // Render Transform Composite Transform X-Axis and Y-Axis Scale
        public static string[] CompositeTransformScaleXY { get; } = new string[] { CompositeTransformScaleX, CompositeTransformScaleY };




        //------------------------------------------------------
        //
        //  Plane Projection
        //
        //------------------------------------------------------

        // Plane Projection X-Axis Rotation
        public static string ProjectionRotationX = "(UIElement.Projection).(PlaneProjection.RotationX)";
        // Plane Projection Y-Axis Rotation
        public static string ProjectionRotationY = "(UIElement.Projection).(PlaneProjection.RotationY)";
        // Plane Projection Z-Axis Rotation
        public static string ProjectionRotationZ = "(UIElement.Projection).(PlaneProjection.RotationZ)";

        public static string ProjectionGlobalOffsetX = "(UIElement.Projection).(PlaneProjection.GlobalOffsetX)";
        public static string ProjectionGlobalOffsetY = "(UIElement.Projection).(PlaneProjection.GlobalOffsetY)";
        public static string ProjectionGlobalOffsetZ = "(UIElement.Projection).(PlaneProjection.GlobalOffsetZ)";

        public static string ProjectionLocalOffsetX = "(UIElement.Projection).(PlaneProjection.LocalOffsetX)";
        public static string ProjectionLocalOffsetY = "(UIElement.Projection).(PlaneProjection.LocalOffsetY)";
        public static string ProjectionLocalOffsetZ = "(UIElement.Projection).(PlaneProjection.LocalOffsetZ)";

        public static string ProjectionCenterOfRotationX = "(UIElement.Projection).(PlaneProjection.CenterOfRotationX)";
        public static string ProjectionCenterOfRotationY = "(UIElement.Projection).(PlaneProjection.CenterOfRotationY)";
        public static string ProjectionCenterOfRotationZ = "(UIElement.Projection).(PlaneProjection.CenterOfRotationZ)";

        public static bool In<T>(this T source, params T[] list)
        {
            if (null == source)
                throw new ArgumentNullException(nameof(source));

            return list.Contains(source);
        }
    }
}
