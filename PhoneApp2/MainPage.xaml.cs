﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media;
using Microsoft.Phone.Controls;

namespace PhoneApp2
{
    public class FillerItem
    {
        public SolidColorBrush Brush { get; set; }
    }

    public class MainPageViewModel : INotifyPropertyChanged
    {
        private string _selectedItem;
        public IList<string> Items { get; set; } = new List<string>();

        public string SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public partial class MainPage : PhoneApplicationPage
    {
        private MainPageViewModel _vm;
        private Random _rand = new Random();

        public MainPage()
        {
            InitializeComponent();

            _vm = new MainPageViewModel { Items = { "one", "two", "three" } };
            _vm.SelectedItem = _vm.Items.Skip(1).First();

            DataContext = _vm;

            Filler.ItemsSource = Enumerable.Range(1, 30)
                .Select(
                    x =>
                        new FillerItem
                        {
                            Brush = new SolidColorBrush(Color.FromArgb(255, (byte)_rand.Next(0, 255), (byte)_rand.Next(0, 255), (byte)_rand.Next(0, 255)))
                        }).ToList();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
        //    if (Dropdown.IsExpanded)
        //    {
        //        Dropdown.Close();
        //    }
        //    else
        //    {
        //        Dropdown.Expand();
        //    }
        }
    }
}